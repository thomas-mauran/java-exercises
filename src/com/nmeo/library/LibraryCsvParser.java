package com.nmeo.library;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class LibraryCsvParser {
    public static void parseBooksFile(String pPath, SafeLibrary pLibrary) throws IOException {
        String line;
        String cvsSplitBy = ",";
        BufferedReader br = new BufferedReader(new FileReader(pPath));
        // Skip first line
        br.readLine();
        while ((line = br.readLine()) != null) {
                // use comma as separator
                String[] bookData = line.split(cvsSplitBy);
                Book newBook = new Book();
                newBook.bookID = Integer.parseInt(bookData[0]);
                newBook.title = bookData[1];
                newBook.authors = bookData[2];
                newBook.averageRating = Double.parseDouble(bookData[3]);
                newBook.isbn = bookData[4];
                newBook.isbn13 = Long.parseLong(bookData[5]);
                newBook.languageCode = bookData[6];
                newBook.numPages = Integer.parseInt(bookData[7]);
                newBook.ratingsCount = Integer.parseInt(bookData[8]);
                newBook.textReviewsCount = Integer.parseInt(bookData[9]);
                newBook.publicationDate = bookData[10];
                newBook.publisher = bookData[11];
                newBook.status = BookStatus.IN_LIBRARY;
                pLibrary.addBook(newBook);
        }
        br.close();
    }

    public static void parseBorrowerFile(String pPath, SafeLibrary pLibrary) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(pPath));
        String line;
        while ((line = br.readLine()) != null) {
            pLibrary.borrowBook(line);
        }
        br.close();
    }

    public static void parseDestroyBook(String pPath, SafeLibrary pLibrary) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(pPath));
        String line;
        while ((line = br.readLine()) != null) {
            pLibrary.destroyBook(line);
        }
        br.close();
    }
}