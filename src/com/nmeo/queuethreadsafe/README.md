# The queue thread safe exercise

In this exercise you'll have to make a queue thread safe.

# First things first

Your first mission is to create a Main in order to:
- Instantiate a UnSafeQueue
- Create a Runnable anonym class in charge of poping 100 stuff from our UnSafeQueue
- Create a Runnable anonym class in charge of adding 100 stuff from our UnSafeQueue
- Create and start 100 threads in charge of popping stuff from the UnSafeQueue
- Create and start 100 threads in charge of adding stuff from the UnSafeQueue

At this point of time you should see some exception appearing !

# Handle the exceptions

From the program you've got with the previous step, handle the exceptions to stop the program nicely in case it happens

# Create a new queue thread safe

The aim here is to fix the issue responsible for the exceptions. Create a new class with an unsafe queue has a member.
Make it thread safe !

# Try to optimize your runtime

Keeping in mind that your program should add 10000 elements in your list and pop them out, try to change the number of thread and the number of element computed by the threads to obtain the best performance for your program.