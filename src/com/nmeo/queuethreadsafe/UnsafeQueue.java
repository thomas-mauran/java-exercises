package com.nmeo.queuethreadsafe;

import java.util.ArrayDeque;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;

import javax.management.RuntimeErrorException;


public class UnsafeQueue<T> {
    private Queue<T> mQueue;
    private AtomicInteger mReadOperationCount;
    private AtomicInteger mWriteOperationCount;

    UnsafeQueue() {
        mQueue = new ArrayDeque<T>();
        mReadOperationCount = new AtomicInteger(0);
        mWriteOperationCount = new AtomicInteger(0);
    }

    Boolean isEmpty() {
        Boolean result;
        mReadOperationCount.incrementAndGet();
        checkSafety();
        result = mQueue.size() == 0;
        mReadOperationCount.decrementAndGet();
        return result;
    }

    int size() {
        int result;
        mReadOperationCount.incrementAndGet();
        checkSafety();
        result = mQueue.size();
        mReadOperationCount.decrementAndGet();
        return result;
    }

    Optional<T> pop() {
        Optional<T> myElement = Optional.empty();
        mWriteOperationCount.incrementAndGet();
        checkSafety();
        if(!mQueue.isEmpty()) {
            myElement = Optional.of(mQueue.poll());
        }
        mWriteOperationCount.decrementAndGet();
        return myElement;
    }

    void add(T item) {
        mWriteOperationCount.incrementAndGet();
        checkSafety();
        mQueue.add(item);
        mWriteOperationCount.decrementAndGet();
    }

    private synchronized void checkSafety() throws RuntimeErrorException {
        if (mWriteOperationCount.get() > 1) {
            throw new RuntimeErrorException( null, "[Multiple writes] multiple calls on critical section");
        }

        if (mWriteOperationCount.get() == 1 && mReadOperationCount.get() > 0) {
            throw new RuntimeErrorException( null, "[Write and read] multiple calls on critical section" );
        }
    }
}
