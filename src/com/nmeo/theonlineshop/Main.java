package com.nmeo.theonlineshop;

class Main {
    public static void main(String[ ] args){
        OnlineShop onlineShop = new OnlineShop(10, 10);
        OnlineShopCli onlineShopCli = new OnlineShopCli(onlineShop);
        onlineShopCli.runCli();
    }
}