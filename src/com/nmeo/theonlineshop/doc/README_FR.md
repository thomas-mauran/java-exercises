# Énoncé initial

# Introduction

Créez un programme Java qui simule une boutique en ligne. La boutique en ligne a des produits à vendre, tels que des livres, des CD, des DVD, etc. Chaque produit a un nom, une description et un prix. Les clients peuvent parcourir les produits et ajouter des articles à leur panier. Une fois qu'ils ont fini leurs achats, ils peuvent passer à la caisse pour finaliser leur commande.

# User story 1: Affichage des produits en stocks et ajout au panier

En tant qu’utilisateur, je souhaite consulter l’état des stocks de la boutique en ligne.
- Depuis le menu principal, je sélectionne le sous menu “Browse products” -> les articles présents s’affichent précédés d’un index avec les informations suivantes
  - Nom
  - Description
  - Prix
  - Quantité en stock
- J’entre l’index correspondant à un article désiré -> l’article est ajouté dans mon panier
- Sur l’index -1, je suis renvoyé au menu principal.

# User story 2: Consulter mon panier

En tant qu’utilisateur, je souhaite consulter les produits qui ont été ajoutés au panier.
- Depuis le menu principal, je sélectionne le sous menu “My Basket” -> les articles précédemment ajoutés s’affichent précédés d’un index avec les informations suivantes
  - Nom
  - Description
  - Prix
- Le coût total de mon panier s’affiche également
- Le menu principal s' affiche automatiquement.

# User story 3: Procéder à l’achat

En tant qu’utilisateur, je souhaite acheter le contenu de mon panier.

- Depuis le menu principal, je sélectionne le sous menu “Purchase Items” -> Un message s’affiche attestant de la translation bancaire et mon panier est vidé

# User story 4: Affichage des services proposés et ajout au panier

En tant qu’utilisateur, je souhaite consulter les services proposés par la boutique en ligne.

- Depuis le menu principal, je sélectionne le sous menu “Browse services” -> les services s’affichent précédés d’un index avec les informations suivantes
  - Nom
  - Description
  - Prix par heure
  - J’entre l’index correspondant à un service désiré -> l’interface me demande combien d’heures je souhaite commander.
  - J’entre le nombre d’heures désirées pour ce service -> Le service est ajouté dans mon panier
  - Sur l’index -1, je suis renvoyé au menu principal.

# User story 5: Suppression d’éléments dans le panier

En tant qu’utilisateur, je souhaite supprimer des articles précédemment ajoutés dans mon panier.

- Depuis le menu principal, je sélectionne le sous menu “My Basket” -> les articles précédemment ajoutés s’affichent précédés d’un index avec les informations suivantes
  - Nom
  - Description
  - Prix
  - -> Le coût total de mon panier s’affiche également suivi de la phrase “Entrer l’index de l’article que vous souhaitez supprimer, sinon entrez -1 pour revenir au menu principal.
- J’entre le numéro X -> l’article correspondant à l’index est supprimé du panier, le panier se réaffiche automatiquement mis à jour.
- J’entre le numéro -1 -> le menu principal s’affiche

# User story 6: Restocker un produit existant

 En tant que gestionnaire, je souhaite restocker un produit

- Depuis le menu principal, j’entre l’index 5 -> la phrase suivante s’affiche “Which product would you like to restock” suivi de la liste des produits contenant (précédés de l’index):
  - Nom
  - Description
  - Prix
  - Quantité en stock.
- J’entre l’index X correspondant au produit à réapprovisionner  -> La phrase suivante s’affiche “How many products would you like to refurbish”
- J’entre le nombre entier Y -> Le menu principal s’affiche
- J’entre l’index 1 “Browse product” -> Le produit à l’index X possède la quantité précédente + Y.

# User story 7: Ajouts de nouveaux produits / services dans le magasin

En tant que gestionnaire, je souhaite ajouter de nouveaux produits / services dans le magasin.

- Depuis le menu principal, j’entre l’index 6 -> la phrase suivante s’affiche “Would you like to restock products (1) or services (2)” ?
- J’entre l’index 1 -> Le questionnaire suivant défile
  - “Please enter the new product’s name”
  - “Please enter the new product’s description”
  - “Please enter the new product’s price”
  - “Please enter the new product’s quantity”
  - “Your product has been added to the stock”
- Le menu principal s’affiche
- J’entre l’index 1 “Browse product” -> Le nouveau produit est présent dans les rayons.
Un fonctionnement similaire doit être implémenté pour les services en remplaçant la quantité de produit et le prix par le coût par heure.

# User story 8: Gestion de plusieurs utilisateurs

En tant qu’utilisateur A, je lance la boutique en ligne:

- J’entre mon nom A et mon prénom AA -> le menu principal s’affiche.
- Je sélectionne le menu “Browse products” puis j’entre l’index X -> le produit X est ajouté à mon panier
- J’entre l’index -1 -> le menu principal s’affiche
- J’entre à nouveau l’index -1 -> Je suis déconnecté du magasin.

L’utilisateur B commence à utiliser la boutique en ligne:

- En tant qu’utilisateur B (sans éteindre le programme suite à l’utilisation de A), j’entre mon nom B et mon prénom BB.
- Je sélectionne le menu “Browse products” puis j’entre l’index X -> le produit X est ajouté à mon panier
- J’entre l’index -1 -> le menu principal s’affiche
- J’entre à nouveau l’index -1 -> Je suis déconnecté du magasin.

L’utilisateur A reprend la main.

- J’entre mon nom A et mon prénom AA -> le menu principal s’affiche.
- Je sélectionne le menu “My basket” -> mon panier s’affiche avec les achats réalisés avant l’utilisation de B

# Description de la state machine dans la CLI

![overview](doc/onlineshopcli.svg)