package com.nmeo.theonlineshop;

public class OnlineShop implements IOnlineShop {
    private final int BASKET_SIZE = 10;

    public OnlineShop(int pMaxProduct, int pMaxServices) {
    }

    public void addProduct(String pName, String pDescription, double pPrice, int pStock) {
        // Create a new product and add it to your product's array
    }

    public void addService(String pName, String pDescription, double pPricePerHour) {
        // Create a new service and add it to your service's array
    }

    public void restockProduct(int pIndex, int pQuantity) {
        // Find the product corresponding to the index and add the quatity to the stock
    }

    public void displayBasketContent() {
        // Display basket content
    }

    public void displayProductsList() {
        // Display products available in the shop
    }

    public void displayServicesList() {
        // Display services available in the shop
    }

    public void addProductToBasket(int pIndex) {
        // Add the product corresponding the index to the basket
        // Verify the index and the stocks !
    }

    public void addServiceToBasket(int pIndex, int pDurationH) {
        // Add the service corresponding the index to the basket
        // Verify the index and calculate the price according to the duration :)
    }

    public void removeArticalFromBasket(int pIndex) {
        // Remove the article from basket
    }

    public void purchaseItems() {
        // Inform the user about the transaction and clear the basket
    }
}
