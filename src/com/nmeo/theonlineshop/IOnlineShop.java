package com.nmeo.theonlineshop;

public interface IOnlineShop {
    public static final int BASKET_SIZE = 10;

    public void addProduct(String pName, String pDescription, double pPrice, int pStock);

    public void addService(String pName, String pDescription, double pPricePerHour);

    public void restockProduct(int pIndex, int pQuantity);

    public void displayBasketContent();

    public void displayProductsList() ;

    public void displayServicesList();

    public void addProductToBasket(int pIndex) ;

    public void addServiceToBasket(int pIndex, int pDurationH);

    public void removeArticalFromBasket(int pIndex);

    public void purchaseItems();
}
