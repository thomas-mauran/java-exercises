package com.nmeo.tictactoe.gameanalyzer;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

// In a prod environment, you want to use a library to perform this operation
public class CsvParser {
    static ArrayList<ArrayList<String>> parseFile(String pPath) {
        ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
        String line = "";
        String cvsSplitBy = ";";
        try (BufferedReader br = new BufferedReader(new FileReader(pPath))) {
            while ((line = br.readLine()) != null) {
                ArrayList<String> myLine = new ArrayList<String>();
                String[] data = line.split(cvsSplitBy);
                for(int i = 0; i < data.length; i++) {
                    myLine.add(data[i]);
                }
                result.add(myLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
