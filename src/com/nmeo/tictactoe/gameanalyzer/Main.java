package com.nmeo.tictactoe.gameanalyzer;

import java.util.Scanner;

import com.nmeo.tictactoe.gameanalyzer.GameAnalyzer.GameState;

class Main {
    public static void main(String[ ] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome to the tic tac toe game analyzer");
        String relativePathToDataSet = args.length == 0 ? "datasets/tictactoe/game1.csv" : args[0];

        GameAnalyzer gameAnalyzer = new GameAnalyzer(CsvParser.parseFile(relativePathToDataSet));
        while(gameAnalyzer.getGameState() == GameState.PLAYING) {
            gameAnalyzer.playNextTurn();
            GameDisplayer.displayGame();
            scanner.nextLine();
        }
        gameAnalyzer.didPlayerWin();
        scanner.close();
    }
}