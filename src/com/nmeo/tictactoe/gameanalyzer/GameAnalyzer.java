package com.nmeo.tictactoe.gameanalyzer;

import java.util.ArrayList;
import java.util.Optional;
import java.util.ArrayDeque;
import java.util.Queue;

public class GameAnalyzer {
    public enum GameState {
        PLAYING,
        FINISHED
    }
    private Queue<Cell> mPlayerX;
    private Queue<Cell> mPlayerO;
    private GameState mGameState;
    private Tocken mPlayerTurn;
    // Create an attribute suitable to host the game's info !

    GameAnalyzer(ArrayList<ArrayList<String>> inputData) {
        mPlayerX = new ArrayDeque<Cell>();
        mPlayerO = new ArrayDeque<Cell>();
        mGameState = GameState.PLAYING;
        mPlayerTurn = Tocken.X;

        for(ArrayList<String> turn : inputData) {
            if(turn.size() != 2) {
                System.err.println("GameAnalyzer: invalid input");
                continue;
            }
            Optional<Cell> cellP1 = Cell.createCellFromString(turn.get(0));
            Optional<Cell> cellP2 = Cell.createCellFromString(turn.get(1));
            if(cellP1.isPresent()) {
                cellP1.get().setTocket(Tocken.X);
                mPlayerX.offer(cellP1.get());
            }
            if(cellP2.isPresent()) {
                cellP2.get().setTocket(Tocken.O);
                mPlayerO.offer(cellP2.get());
            }
        }
    }

    public void playNextTurn() {
        // Play the next move
    }

    public void didPlayerWin() {
        // Determine if somebody won and display the result
    }

    public GameState getGameState() {
        return mGameState;
    }
}
