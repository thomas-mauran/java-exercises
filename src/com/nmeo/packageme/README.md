# Package me

Package me trains your software architecture skills. In this exercice, you'll training to
- Compile code without IDE
- Organize your code in packages
- Generate JAR archive

## 1-Compile

First of all try to compile and execute packageme using only your terminal.

## 2-Refactoring

Your second task would be to refactor all this code to get it into packages. Compile and run the code again after reorgazing.

## 3-Generate JAR archive

Get your JAR archive out of your code and execute it.

## 4-Generate Java Doc

Get your java documentation (the code has been prepared before hand) except for 2 classes ;).
