package com.nmeo.packageme;

import java.util.UUID;

/**
 * The abstract class Article represents a generic article with a name, description, price, and UUID.
 */
public abstract class Article {
    protected String mName; // Name of the article
    protected String mDescription; // Description of the article
    protected double mPrice; // Price of the article
    protected UUID mUuid; // UUID of the article

    /**
     * Constructor for Article class.
     *
     * @param pName        Name of the article.
     * @param pDescription Description of the article.
     * @param pPrice       Price of the article.
     */
    public Article(String pName, String pDescription, double pPrice) {
        mName = pName;
        mDescription = pDescription;
        mPrice = pPrice;
        mUuid = UUID.randomUUID();
    }

    /**
     * Get the name of the article.
     *
     * @return The name of the article.
     */
    public String getName() {
        return mName;
    }

    /**
     * Set the name of the article.
     *
     * @param pName The name to set for the article.
     */
    public void setName(String pName) {
        mName = pName;
    }

    /**
     * Get the description of the article.
     *
     * @return The description of the article.
     */
    public String getDescription() {
        return mDescription;
    }

    /**
     * Set the description of the article.
     *
     * @param pDescription The description to set for the article.
     */
    public void setDescription(String pDescription) {
        mDescription = pDescription;
    }

    /**
     * Get the price of the article.
     *
     * @return The price of the article.
     */
    public double getPrice() {
        return mPrice;
    }

    /**
     * Set the price of the article.
     *
     * @param pPrice The price to set for the article.
     */
    public void setPrice(double pPrice) {
        mPrice = pPrice;
    }

    /**
     * Get the UUID of the article.
     *
     * @return The UUID of the article.
     */
    public UUID getUuid() {
        return mUuid;
    }
}