# Java Exercises

This project contains exercices to train your JAVA POO skills !

This project is organized toward 2 branches
- boiler-plate contains your stating point
- solution contains the solution I design (yours can ne really different and still correct ;) )

# The online shop

The online shop trains your UML conception skills as well as your java basis. Classes, heritance, interfaces are the concepts you will using !
Please find associated README here.

# Package me

Package me trains your software architecture skills. In this exercice, you'll be training to
- Compile code without IDE
- Organize your code in packages
- Generate JAR archive

# Tic tac toe game analyzer

Tic tac toe game analyzer trains your collection skills. In this exercice, you'll be training to
- Use collection methods
- Choose the best collection according to your use case
- Get better in tic tac toe's tactics !


# Queue thread safe

This exercice will help you train on your multithreading skills ! We will go through the basics of multithreading in Java !

# The library exercise

In this exercise, you'll training on exceptions, collections, streams and multithreading